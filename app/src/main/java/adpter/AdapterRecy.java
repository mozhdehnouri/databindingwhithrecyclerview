package adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.databinding.ShowitemsBinding;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import userModel.User;

public class AdapterRecy extends RecyclerView.Adapter<AdapterRecy.MyViewHolder> {
    public AdapterRecy(Context context, List<User> mylist) {
        this.context = context;
        this.mylist = mylist;
    }

    Context context;
    List<User> mylist;


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ShowitemsBinding showitemsBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.showitems,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(showitemsBinding);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        User user=mylist.get(position);
        holder.showitemsBinding.setUser(user);

    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
     ShowitemsBinding showitemsBinding;
        public MyViewHolder(@NonNull ShowitemsBinding itemView) {
            super(itemView.getRoot());
            showitemsBinding=itemView;
        }
    }
}
