package userModel;

import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

public class User {
     int image;
     String name;
     String email;

    public User(int image, String name, String email) {
        this.image = image;
        this.name = name;
        this.email = email;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @BindingAdapter("android:imageUrl")
    public static void loadImage(View v,int imagebase){
        CircleImageView circleImageView=(CircleImageView) v;
        circleImageView.setImageDrawable(ContextCompat.getDrawable(v.getContext(),imagebase));
    }


}
