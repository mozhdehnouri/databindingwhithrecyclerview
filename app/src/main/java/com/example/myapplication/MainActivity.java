package com.example.myapplication;

import adpter.AdapterRecy;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import userModel.User;

import android.os.Bundle;

import com.example.myapplication.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AdapterRecy adapterRecy;
    private List<User> myuser = new ArrayList<>();
    ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        adddata();
        showdata();
    }

    private void adddata() {
        myuser.add(new User(R.drawable.man, "mozhdeh", "MozhdehNouri@yahoo.com"));
        myuser.add(new User(R.drawable.dn, "Zahra", "Zahra@yahoo.com"));
        myuser.add(new User(R.drawable.person, "Zahra", "Zahra@yahoo.com"));
        myuser.add(new User(R.drawable.dn, "Naghmeh", "Naghmeh@yahoo.com"));
        myuser.add(new User(R.drawable.dn, "Shadi", "Shadi@yahoo.com"));
        myuser.add(new User(R.drawable.person, "Mohhamd", "Mohhamd@yahoo.com"));
        myuser.add(new User(R.drawable.dn, "Mozhgan", "Mozhgan@yahoo.com"));
        myuser.add(new User(R.drawable.person, "Ali", "Ali@yahoo.com"));
        myuser.add(new User(R.drawable.dn, "Zahra ", "Zahrapormy@yahoo.com"));
        myuser.add(new User(R.drawable.person, "Mojtaba", "Zahra@yahoo.com"));
        myuser.add(new User(R.drawable.dn, "Maryam", "Maryam@yahoo.com"));
        myuser.add(new User(R.drawable.person, "Omid", "Omid@yahoo.com"));

    }

    private void showdata() {
        activityMainBinding.recyclerview.setLayoutManager(new LinearLayoutManager(this));
        adapterRecy = new AdapterRecy(this, myuser);
        activityMainBinding.recyclerview.setAdapter(adapterRecy);
        activityMainBinding.recyclerview.setHasFixedSize(true);
        RecyclerView.ItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        activityMainBinding.recyclerview.addItemDecoration(divider);
    }
}
